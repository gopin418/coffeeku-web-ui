# CoffeeKu

## Project setup and build
```
docker-compose up --build
```

### Compiles and hot-reloads for development
```
docker-compose up
```

### Lints and fixes files
```
yarn lint
```