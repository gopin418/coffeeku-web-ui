# develop stage
FROM node:14.15.1

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package.json /app/package.json
RUN yarn install
RUN yarn add @vue/cli@3.7.0 -g
COPY . .

# start app
CMD ["yarn", "serve"]